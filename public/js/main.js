$(document).ready(function () {

    // Add listener event for homepage header transition
    $(function() {
        //caches a jQuery object containing the header element
        let header = $("header");
        $(window).scroll(function() {
            let scroll = $(window).scrollTop();

            if (scroll >= 5) {
                header.addClass('scroll');
            } else {
                header.removeClass('scroll');
            }
        });
    });



    let humBtn = $('.animated-icon')
    $('.second-button').on('click', function (e) {
        // prevent double click
        // e.preventDefault();
        // let el = $(this);
        // el.prop('disabled', true);
        // setTimeout(function(){el.prop('disabled', false); }, 3000);
        //

        let menuItem = $( e.currentTarget );

        if (menuItem.attr( 'aria-expanded') === 'true' && !humBtn.hasClass('open')) {
            $('li.collapsed-logo').children('a').css("display", "block")
            $('a.navbar-brand').children('img').css("opacity", "0")

            $('body').css("overflow-y", "hidden")
            humBtn.toggleClass('open');

        } else {
            $('li.collapsed-logo').children('a').css("display", "none")
            $('a.navbar-brand').children('img').css("opacity", "1")
            $('body').css("overflow-y", "auto")
            humBtn.removeClass('open');

        }
    });



//    form validation
    (function () {

        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        let forms = document.querySelectorAll('.needs-validation')

        // Loop over them and prevent submission
        Array.prototype.slice.call(forms)
            .forEach(function (form) {
                form.addEventListener('submit', function (event) {
                    if (!form.checkValidity()) {
                        event.preventDefault()
                        event.stopPropagation()
                        console.log(forms)
                    }

                    form.classList.add('was-validated')
                }, false)
            })
    })()


    // Data picker features

    Date.prototype.toDateInputValue = (function() {
        let local = new Date(this);
        local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
        return local.toJSON().slice(0,10);
    });

    let dateInput = document.getElementById('validationCustom01');
    dateInput = "1111-01-01";

    if (dateInput.value !== null) {
        dateInput.value = new Date().toDateInputValue();
    }


    $("#validationCustom01").on("change", function() {
        this.setAttribute(
            "data-date",
            moment(this.value, "YYYY-MM-DD")
                .format( this.getAttribute("data-date-format") )
        )
    }).trigger("change")

    // phone validation
    $(function(){
        $("#validationCustom03").mask("+99 (9) 999 99999999");
    });


//    mute & unmute video on About page as well as Corporate page
    $(".muteBtn-about").click(function () {
        if ($("#video-about-page").prop('muted')) {
            $("#video-about-page").prop('muted', false);
            // changing icon for button
            $(this).children().removeClass('bi-volume-mute');
            $(this).children().addClass('bi-volume-up');
            // changing border for button
            $(this).addClass('active-sound');

        } else {
            $("#video-about-page").prop('muted', true);
            // changing icon for button
            $(this).children().removeClass('bi-volume-up');
            $(this).children().addClass('bi-volume-mute');
            $(this).removeClass('active-sound');
        }
        console.log($("video").prop('muted'))
    });
    $(".muteBtn-corp").click(function () {
        if ($("#video-corp-page").prop('muted')) {
            $("#video-corp-page").prop('muted', false);
            // changing icon for button
            $(this).children().removeClass('bi-volume-mute');
            $(this).children().addClass('bi-volume-up');
            // changing border for button
            $(this).addClass('active-sound');

        } else {
            $("#video-corp-page").prop('muted', true);
            // changing icon for button
            $(this).children().removeClass('bi-volume-up');
            $(this).children().addClass('bi-volume-mute');
            $(this).removeClass('active-sound');
        }
        console.log($("video").prop('muted'))
    });
});